﻿using System;
using System.Collections.Generic;
using System.Messaging;


namespace ServiceClientMSMQ
{
    class Program
    {
        const string _queuName = ".\\private$\\MyNewPrivateQueue";
        private static MessageQueue orderQueue;

        static void Main(string[] args)
        {
            orderQueue = new MessageQueue(_queuName);
            orderQueue.Formatter = new XmlMessageFormatter(
                new Type[]
                {
                    typeof(string)
                });

            SendMessage("Hello world", "First queue application");

            var messages = GetMessages();

            foreach(var message in messages)
            {
                Console.WriteLine($"Message= {message}");
            }

            Console.ReadLine();
        }

        private static void SendMessage(string message, string lable)
        {
            try
            {
                if (!MessageQueue.Exists(_queuName))
                {
                    CreateMSMQ();
                }

                var queue = new MessageQueue(_queuName);
                queue.Send(message, lable);
            }
            catch (MessageQueueException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }


        private static List<string> GetMessages()
        {
            var result = new List<string>();

            using (MessageEnumerator messagesEnumerator = orderQueue.GetMessageEnumerator2())
            {
                while (messagesEnumerator.MoveNext(TimeSpan.FromSeconds(30)))
                {
                    var mes = messagesEnumerator.Current.Body.ToString();
                    result.Add(mes);
                }
            }

            return result;
        }

        private  static void CreateMSMQ()
        {
            using (var queue = MessageQueue.Create(_queuName))
            {
                queue.Label = "Demo Queue";
                Console.WriteLine("Очередь создана:");
                Console.WriteLine("Путь: {0}", queue.Path);
                Console.WriteLine("Форматное имя: {0}", queue.FormatName);
            }
        }
    }
    
}
